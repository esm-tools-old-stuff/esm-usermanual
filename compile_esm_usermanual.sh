#!/usr/bin/env bash
set -x
#tex forest.ins
#./extract_default_values.sh
pdflatex esm_usermanual.tex
makeindex esm_usermanual
makeglossaries esm_usermanual
bibtex esm_usermanual.aux
pdflatex esm_usermanual.tex
pdflatex esm_usermanual.tex
